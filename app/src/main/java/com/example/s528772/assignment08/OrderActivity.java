package com.example.s528772.assignment08;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class OrderActivity extends AppCompatActivity {

    Integer value;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
         TextView orderNum = (TextView) findViewById(R.id.textView11);
        Random rand = new Random();
        Integer n = rand.nextInt(200);
        String ordernumber = Integer.toString(n);
        orderNum.setText(ordernumber);
//        SharedPreferences y = PreferenceManager.getDefaultSharedPreferences(OrderActivity.this);
//        value = y.getInt("price", 0);

    }

    public void getPrice(View v){
        SharedPreferences y = PreferenceManager.getDefaultSharedPreferences(OrderActivity.this);
        EditText boxescountET = (EditText)findViewById(R.id.editText5);
        if(boxescountET.getText().toString().length()>0){
            int boxesCount = Integer.parseInt(boxescountET.getText().toString());
            TextView priceTV = (TextView)findViewById(R.id.textView13);
            value=y.getInt("cost",0);
            priceTV.setText(String.valueOf(value*boxesCount));
        } else {
            Toast.makeText(getApplicationContext(), "Enter Number of boxes", Toast.LENGTH_SHORT).show();
        }


    }


    public void placeOrder(View v){
        EditText nameET = (EditText)findViewById(R.id.editText3);
        EditText cookietypeET = (EditText)findViewById(R.id.editText4);
        EditText boxescountET = (EditText)findViewById(R.id.editText5);
        TextView priceTV = (TextView)findViewById(R.id.textView13);
        String name = nameET.getText().toString();
        String cookietype = cookietypeET.getText().toString();
        String nofboxes = boxescountET.getText().toString();
        String totalprice = priceTV.getText().toString();

        if(name.equals("") || cookietype.equals("") || nofboxes.equals("")){
            Toast.makeText(getApplicationContext(), "Enter the three fields", Toast.LENGTH_SHORT).show();
        } else if(totalprice.equals("")){
            Toast.makeText(getApplicationContext(), "click on Get price", Toast.LENGTH_SHORT).show();
        } else {
            int boxescount = Integer.parseInt(nofboxes);
            int price = Integer.parseInt(totalprice);
            DatabaseOpenHelper helper = new DatabaseOpenHelper(getApplicationContext(), 1);
            SQLiteDatabase orderDB = helper.getWritableDatabase();
            ContentValues order = new ContentValues();

            order.put("totalprice", price);
            order.put("nofboxes", boxescount);
            order.put("cookietype", cookietype);
            order.put("name", name);
            orderDB.insert("orders", null, order);

            Cursor results = orderDB.query("orders", new String[]{"_id2", "name", "cookietype", "nofboxes", "totalprice"}, null, null, null, null, null);
            for(int i = 0; i<results.getCount(); i++)
            {
                results.moveToPosition(i);
                System.out.println(results.getInt(0) + " " + results.getString(1) + " " + results.getString(2) + " " + results.getInt(3) + " " + results.getInt(4));
            }
            helper.close();
        }

    }

    public void printorder(View v){

        DatabaseOpenHelper helper = new DatabaseOpenHelper(getApplicationContext(), 1);
        SQLiteDatabase orderDB = helper.getWritableDatabase();

        Cursor results = orderDB.query("orders", new String[]{"_id2", "name", "cookietype", "nofboxes", "totalprice"}, null, null, null, null, null);
        for(int i = 0; i<results.getCount(); i++)
        {
            results.moveToPosition(i);
            System.out.println(results.getInt(0) + " " + results.getString(1) + " " + results.getString(2) + " " + results.getInt(3) + " " + results.getInt(4));
        }

        helper.close();

    }

}

