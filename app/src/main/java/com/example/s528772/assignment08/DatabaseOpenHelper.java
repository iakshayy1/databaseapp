package com.example.s528772.assignment08;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by s528772 on 11/9/2017.
 */

public class DatabaseOpenHelper extends SQLiteOpenHelper {
    public DatabaseOpenHelper(Context context) {

        super(context, "cookieDB", null,1);

    }

    public DatabaseOpenHelper(Context context, Integer x) {

        super(context, "orderDB", null, 1);

    }

    private static final String createDB = "create table cookie(" + "_id integer primary key autoincrement," +
            "price integer," +
            "color integer)";

    private static final String createDB2 = "create table orders(" + "_id2 integer primary key autoincrement," +
            "name text not null," +
            "cookietype text not null," +
            "nofboxes integer," +
            "totalprice integer)";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        try{
            sqLiteDatabase.execSQL(createDB);
            sqLiteDatabase.execSQL(createDB2);
        } catch (Exception e){
            Log.d("cookieDB",e.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {


        sqLiteDatabase.execSQL("Drop table if exists orders");

        onCreate(sqLiteDatabase);
    }

}
