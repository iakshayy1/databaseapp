package com.example.s528772.assignment08;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    static Integer pricecookies;
    static Integer colour;
    Activity activity=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.drawable.shoppingcart);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText price= (EditText) findViewById(R.id.editText);
                String pri = price.getText().toString();
                EditText color = (EditText)findViewById(R.id.editText2);
                String color1=color.getText().toString();
                if(!pri.isEmpty()&&!color1.isEmpty()) {
                Intent i = new Intent(MainActivity.this,OrderActivity.class);

                startActivity(i);
            }
            else{

                    Toast.makeText(getApplicationContext(), "Please enter the numbers", Toast.LENGTH_SHORT).show();
                }
        }});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void update(View view){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sp.edit();

        EditText price= (EditText) findViewById(R.id.editText);
        String pri = price.getText().toString();
        EditText color = (EditText)findViewById(R.id.editText2);
        String color1=color.getText().toString();
        if(!pri.isEmpty()&&!color1.isEmpty()) {
            colour = Integer.parseInt(color1);
            pricecookies = Integer.parseInt(pri);

            editor.putInt("cost", (int) pricecookies);
            editor.putInt("colour", colour);
            editor.apply();
            editor.commit();
            if (colour < 5 && colour > 0) {
                if (colour == 1) {
                    activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
                }
                if (colour == 2) {
                    activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                }
                if (colour == 3) {
                    activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                }
                if (colour == 4) {
                    activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.Blue));
                }
            } else {
                Toast.makeText(getApplicationContext(), "pick a number from 1 to 4", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "Please enter the numbers", Toast.LENGTH_SHORT).show();
        }



    }
}
